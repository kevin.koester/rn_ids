module HTTP;

const twitter_token_names = set("_twitter_sess", "auth_token");

## We are using ip and user agent as an idicator for potential attacks
global cookies_addr: table[string] of addr;
global cookies_agent: table[string] of string;

export {
	# Add cookie field
	redef record Info += {
		cookie: string &log &optional;
	};
	
	redef enum Notice::Type += {
		TwitterHijack
	};
}


# Save all cookies
event http_header(c: connection, is_orig: bool, name: string, value: string){
	if (is_orig && name == "COOKIE") {
		c$http$cookie = value;
	}
}

## This returns a tring to simplyfy searching
function extract_session_auth(cookie: string) : string {
	## XXX: the " " is important!
	local fields = split(cookie, /; /);
	local results: table[string] of string;
	local result_string = "";
	for(i in fields){
		## split_string results in different behavior
		local sp = split(fields[i], /=/);
		if (sp[1] in twitter_token_names) {
			results[sp[1]] = sp[2];
		}
	}
	if(|results| == |twitter_token_names|){
		for (name in twitter_token_names){
			result_string += name;
			result_string += "=";
			result_string += results[name];
			result_string += ";";
		}
	}
	return result_string;
}


event http_all_headers(c: connection, is_orig: bool, header_list: mime_header_list) {
	if (!is_orig || !c$http?$cookie || c$http$cookie == "") { 
		return;
	}
	
	## Ignore all non-Twitter
	if(/twitter.com/ !in c$http$host){
		return;
	}
	
	local cookie = extract_session_auth(c$http$cookie);
	
	## First time seeing this cookie
	if(cookie !in cookies_addr) {
		cookies_addr[cookie] = c$id$orig_h;
		cookies_agent[cookie] = c$http$user_agent;
		return;
	}
	## Second time seeing this cookie
	if(cookies_addr[cookie] == c$id$orig_h && cookies_agent[cookie] == c$http$user_agent){
		return;
	}
	
	## Session hijacker!
	print fmt("Found a session hijacker! IP: %s with useragent: %s", c$id$orig_h, c$http$user_agent);
	
	NOTICE([
		$note = TwitterHijack,
		$msg=fmt("Found a session hijacker! IP: %s with useragent: %s", c$id$orig_h, c$http$user_agent),
		$suppress_for=5min,
		$conn=c,
		$identifier=cookie
	]);
}

