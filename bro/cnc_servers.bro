@load base/protocols/dns/main.bro

export {
	redef enum Notice::Type += {
		CNCScan
	};
}

global clean_ips: set[addr];
global cnc_servers: set[addr];

function add_clean_ip(a: addr){
	add clean_ips[a];
	if(a in cnc_servers){
		print fmt("%s was probably not a cnc server. We just missed the dns request", a);
		delete cnc_servers[a];
		NOTICE([
		$note = CNCScan,
		$msg=fmt("%s was probably not a cnc server. We just missed the dns request", a)
	]);
	}
}

event dns_A_reply(c: connection, msg: dns_msg, ans: dns_answer, a:addr){
	add_clean_ip(a);
	add_clean_ip(c$id$orig_h); ## We are assuming dns servers are not evil
	add_clean_ip(c$id$resp_h); ## Adding the dns request source as clean. We probably won't capture the cnc traffic to a dns server.
}

function check_cnc(c: connection){
	if(c$id$resp_h !in clean_ips){
		print fmt("%s is a possible CNC server over port  %s!", c$id$resp_h, c$id$resp_p);
		add cnc_servers[c$id$resp_h];
		NOTICE([
		$note = CNCScan,
		$msg=fmt("Possible cnc server: %s", c$id$resp_h),
		$conn=c
	]);
	}
}

## use tcp_packet, to get every connection
event tcp_packet(c: connection, is_orig: bool, flags: string, seq: count, ack: count, len: count, payload: string){
	check_cnc(c);
}

event udp_request(c: connection){
	check_cnc(c);
}
