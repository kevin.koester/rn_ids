/*
 * pcap_analyzer.h
 *
 *  Created on: 17 Dec 2018
 *      Author: kevin
 */

#ifndef SRC_PCAP_ANALYZER_H_
#define SRC_PCAP_ANALYZER_H_

#include <pcap.h>

#include <algorithm>
#include <cstring>
#include <map>
#include <memory>
#include <numeric>
#include <string>
#include <tuple>
#include <unordered_map>
#include <unordered_set>
#include <vector>

enum PACKET_TYPE {
	IP = (1 << 0),
	TCP = (1 << 1),
	UDP = (1 << 2),
	ICMP = (1 << 3),
	OTHER = (1 << 4),
};

struct proto_header {
	int proto_type = PACKET_TYPE::IP;

	virtual ~proto_header() {}
};

struct proto_header_tcp : public tcphdr, public proto_header {
	proto_header_tcp(const tcphdr& tcp) : tcphdr(tcp) {
		this->proto_type = PACKET_TYPE::TCP;
	}
};

struct proto_header_udp : public udphdr, public proto_header {
	proto_header_udp(const udphdr& udp) : udphdr(udp) {
		this->proto_type = PACKET_TYPE::UDP;
	}
};

struct proto_header_icmp : public icmphdr, public proto_header {
	proto_header_icmp(const icmphdr& icmp) : icmphdr(icmp) {
		this->proto_type = PACKET_TYPE::ICMP;
	}
};

struct pckt_header {
	iphdr ip;
	std::shared_ptr<proto_header> proto;
	std::shared_ptr<uint8_t> data;

	uint64_t total_ip_length = 0;
};

typedef std::tuple<uint32_t, uint16_t> ip_port_t;
typedef std::tuple<ip_port_t, ip_port_t> ip_port_flow_t;
// TODO: unordered to make it faster?
typedef std::map<ip_port_flow_t, std::vector<std::shared_ptr<pckt_header>>>
	ip_port_flow_map_t;

typedef ip_port_flow_t tcp_connection_t;
typedef std::multimap<tcp_connection_t,
					  std::vector<std::shared_ptr<pckt_header>>>
	tcp_connection_list_t;

class PcapAnalyzer {
 public:
	PcapAnalyzer(const std::string& filename, const int max_packets = 0);

	uint64_t get_total_packets() const { return this->m_total_packets; }

	uint64_t get_num_ips_ipv4() const {
		std::unordered_set<uint32_t> addrs;
		for (auto iter = this->m_ip_headers.begin();
			 iter != this->m_ip_headers.end(); ++iter) {
			if ((*iter)->ip.version == 4) {
				addrs.insert((*iter)->ip.saddr);
				addrs.insert((*iter)->ip.daddr);
			}
		}
		return addrs.size();
	}

	/**
	 * Gets the number of total ipv4 packets
	 * @return
	 */
	uint64_t get_num_packets_ipv4() const {
		return std::accumulate(
			this->m_ip_headers.begin(), this->m_ip_headers.end(), (uint64_t)0,
			[](uint64_t sum, std::shared_ptr<pckt_header> header) {
				if (header->ip.version == 4) ++sum;
				return sum;
			});
	}

	/**
	 * Gets the total payload size in byte
	 * @param type packet type to get the package size from
	 * @return
	 */
	uint64_t get_total_payload_size(int type = PACKET_TYPE::IP) const {
		return std::accumulate(
			this->m_ip_headers.begin(), this->m_ip_headers.end(), (uint64_t)0,
			[type](uint64_t sum, std::shared_ptr<pckt_header> header) {
				if (type == PACKET_TYPE::IP ||
					(type == PACKET_TYPE::TCP &&
					 header->ip.protocol == IPPROTO_TCP) ||
					(type == PACKET_TYPE::UDP &&
					 header->ip.protocol == IPPROTO_UDP) ||
					(type == PACKET_TYPE::OTHER &&
					 header->ip.protocol != IPPROTO_UDP &&
					 header->ip.protocol != IPPROTO_TCP)) {
					sum += ::ntohs(header->ip.tot_len);
				}
				return sum;
			});
	}

	/**
	 * Get all packets with size > max_size and no fragmentation bit set
	 * @param max_size
	 * @return
	 */
	std::vector<std::shared_ptr<pckt_header>> find_unsusual_packet_size() {
		std::vector<std::shared_ptr<pckt_header>> suspicious_packets;
		for (auto iter = this->m_ip_headers.begin();
			 iter != this->m_ip_headers.end(); ++iter) {
			auto ip_len = ::ntohs((*iter)->ip.tot_len);
			// FIXME: calculation won't work with ICMP packets. They are 60byte
			// with 32byte tot_len?
			if (ip_len > 100 && ip_len != (*iter)->total_ip_length) {
				suspicious_packets.push_back(*iter);
			}
		}
		return suspicious_packets;
	}

	/**
	 * Returns the hosts with all suspicious packets
	 * @param suspicious_packets
	 * @return map<ip, list of packets>
	 */
	std::map<uint32_t, std::vector<std::shared_ptr<pckt_header>>>
	get_unsusual_senders(
		const std::vector<std::shared_ptr<pckt_header>>& suspicious_packets) {
		std::map<uint32_t, std::vector<std::shared_ptr<pckt_header>>> hosts;

		for (auto iter = suspicious_packets.begin();
			 iter != suspicious_packets.end(); ++iter) {
			std::shared_ptr<pckt_header> packet = *iter;
			hosts[packet->ip.saddr].push_back(packet);
		}
		return hosts;
	}

	/**
	 * Get the flows for the given package type
	 * @param type
	 * @return
	 */
	ip_port_flow_map_t get_ip_flows(int type) const {
		ip_port_flow_map_t flow_map;
		for (auto iter = this->m_ip_headers.begin();
			 iter != this->m_ip_headers.end(); ++iter) {
			// Find flow from a to b or b to a in flow_map
			auto find_iter = flow_map.end();
			ip_port_t from = {(*iter)->ip.saddr, 0};
			ip_port_t to = {(*iter)->ip.daddr, 0};

			// Add ports if applicable
			if (type & (1 << PACKET_TYPE::TCP)) {
				auto tcp_hdr =
					std::dynamic_pointer_cast<proto_header_tcp>((*iter)->proto);
				if (tcp_hdr) {
					std::get<1>(from) = tcp_hdr->th_sport;
					std::get<1>(to) = tcp_hdr->th_dport;
				}
			}
			if (type & (1 << PACKET_TYPE::UDP)) {
				auto udp_hdr =
					std::dynamic_pointer_cast<proto_header_udp>((*iter)->proto);
				if (udp_hdr) {
					std::get<1>(from) = udp_hdr->uh_sport;
					std::get<1>(to) = udp_hdr->uh_dport;
				}
			}

			if ((find_iter = flow_map.find({from, to})) != flow_map.end() ||
				(find_iter = flow_map.find({to, from})) != flow_map.end()) {
				// Found flow and add packet to list
				find_iter->second.push_back(*iter);
			} else {
				// new flow
				flow_map[{from, to}].push_back(*iter);
			}
		}
		return flow_map;
	}

	/**
	 * Get all tcp connections
	 * @return
	 */
	tcp_connection_list_t get_tcp_connections() const {
		std::map<tcp_connection_t, std::vector<std::shared_ptr<pckt_header>>>
			open_connections;
		tcp_connection_list_t closed_connections;
		for (auto iter = this->m_ip_headers.begin();
			 iter != this->m_ip_headers.end(); ++iter) {
			auto tcp_header =
				std::dynamic_pointer_cast<proto_header_tcp>((*iter)->proto);

			if (tcp_header) {
				ip_port_flow_t flow = {{(*iter)->ip.saddr, tcp_header->source},
									   {(*iter)->ip.daddr, tcp_header->dest}};

				// new connection
				if (tcp_header->syn) {
					auto res = open_connections.find(flow);
					// Close old connection
					if (res != open_connections.end()) {
						bool new_connection = true;
						// Checking for retransmission
						if (tcp_header->check != 0 && res->second.size() > 0) {
							auto res_tcp_hdr =
								std::static_pointer_cast<proto_header_tcp>(
									(*(res->second.end() - 1))->proto);
							if (tcp_header->check == res_tcp_hdr->check) {
								// likely a retransmission so don't create a new
								// connection
								new_connection = false;
							}
						}
						if (new_connection) {
							closed_connections.insert(
								{res->first, res->second});
							open_connections.erase(res);
						}
					}
					// Add new connection with one packet
					open_connections[flow].push_back(*iter);
				} else if (tcp_header->fin || tcp_header->rst) {
					auto res = open_connections.find(flow);
					open_connections[flow].push_back(*iter);
					// Close connection
					if (res != open_connections.end()) {
						closed_connections.insert({res->first, res->second});
						open_connections.erase(res);
					}
				} else {
					open_connections[flow].push_back(*iter);
				}
			}
		}

		for (auto iter = open_connections.begin();
			 iter != open_connections.end(); ++iter) {
			// close all connections
			closed_connections.insert({iter->first, iter->second});
		}
		open_connections.clear();

		return closed_connections;
	}

	/**
	 * Get all flows, which are probably part of a syn scan
	 * @param tcp_connections
	 * @return
	 */
	std::vector<std::shared_ptr<pckt_header>> get_syn_scanner_packets(
		const tcp_connection_list_t& tcp_connections) const {
		// TODO: why map? every connection should be unique
		std::vector<std::shared_ptr<pckt_header>> scan_map;
		// Iterate through all connections
		for (auto connection_iter = tcp_connections.begin();
			 connection_iter != tcp_connections.end(); ++connection_iter) {
			bool found_ack = false;

			// Check if first packet is a syn packet (there may be
			// connections without syn due to incomplete capture)
			if (connection_iter->second.size() > 0) {
				auto packet_iter = connection_iter->second.begin();
				auto tcp_syn_hdr = std::dynamic_pointer_cast<proto_header_tcp>(
					(*packet_iter)->proto);
				if (!(tcp_syn_hdr && tcp_syn_hdr->syn && !tcp_syn_hdr->ack)) {
					// first packet is no syn packet. go to next connection
					continue;
				}

				if (connection_iter->second.size() >= 3) {
					for (packet_iter = connection_iter->second.begin() + 1;
						 packet_iter != connection_iter->second.end();
						 ++packet_iter) {
						auto possible_ack_hdr =
							std::dynamic_pointer_cast<proto_header_tcp>(
								(*packet_iter)->proto);
						// searching for an ack packet without syn or reset
						// should be sufficient
						if (possible_ack_hdr && possible_ack_hdr->ack &&
							!possible_ack_hdr->syn && !possible_ack_hdr->rst) {
							found_ack = true;
						}
					}
					// This won't find retransmissions etc
					/*
					auto tcp_syn_ack_hdr =
						std::dynamic_pointer_cast<proto_header_tcp>(
							(*(packet_iter + 1))->proto);
					auto tcp_ack_hdr =
						std::dynamic_pointer_cast<proto_header_tcp>(
							(*(packet_iter + 2))->proto);

					// TODO: check direction and seq
					if (tcp_syn_ack_hdr && tcp_ack_hdr &&
						(tcp_syn_ack_hdr->ack && tcp_syn_ack_hdr->syn) &&
						tcp_ack_hdr->ack) {
						found_ack = true;
					}*/
				}
				if (!found_ack) {
					auto packet = connection_iter->second[0];
					scan_map.push_back(packet);
				}
			}
		}
		return scan_map;
	}

	/**
	 * Sort a list of scan results (udp or syn)
	 * @param syn_scanners
	 * @return
	 */
	std::vector<std::tuple<uint32_t, uint64_t>> sort_scanners(
		std::map<uint32_t, uint64_t> syn_scanners) {
		std::vector<std::tuple<uint32_t, uint64_t>> sorted;
		std::copy(syn_scanners.begin(), syn_scanners.end(),
				  std::back_inserter(sorted));
		std::sort(sorted.begin(), sorted.end(),
				  [](std::tuple<uint32_t, uint64_t> a,
					 std::tuple<uint32_t, uint64_t> b) {
					  return std::get<1>(a) > std::get<1>(b);
				  });
		return sorted;
	}

	/**
	 * Get the UDP packages, which are part of a portscan
	 * @return
	 */
	std::vector<std::shared_ptr<pckt_header>> get_udp_scanner_packets() {
		std::vector<std::shared_ptr<pckt_header>> udp_packets;
		std::vector<std::shared_ptr<pckt_header>> scan_packets;
		for (auto iter = this->m_ip_headers.begin();
			 iter != this->m_ip_headers.end(); ++iter) {
			if ((*iter)->ip.protocol == IPPROTO_UDP) {
				udp_packets.push_back(*iter);
			} else if ((*iter)->ip.protocol == IPPROTO_ICMP) {
				auto icmp_header = std::dynamic_pointer_cast<proto_header_icmp>(
					(*iter)->proto);
				// ICMP unreachable
				if (icmp_header && icmp_header->type == 3 &&
					icmp_header->code == 3) {
					if ((*iter)->data.get() == nullptr) {
						continue;
					}
					// Extract ip packet from icmp response
					iphdr* ip_from_icmp =
						reinterpret_cast<iphdr*>((*iter)->data.get());
					if (ip_from_icmp->protocol != 17) continue;
					udphdr* udp_from_icmp = reinterpret_cast<udphdr*>(
						(*iter)->data.get() + sizeof(iphdr));
					pckt_header scan_header;
					scan_header.ip = *ip_from_icmp;
					scan_header.proto =
						std::make_shared<proto_header_udp>(*udp_from_icmp);
					scan_packets.push_back(
						std::make_shared<pckt_header>(scan_header));
					// if (udp_from_icmp) scan_packets.push_back(*iter);

					// find corresponding udp packets and copy them to
					// scan_packets. using copy_if to copy all matching udp
					// packets (in case of flooding etc)

					// TODO: this seems to be needless. We already have the udp
					// packet... keeping this in case we need to find the actual
					// packet
					/*
					std::copy_if(
						udp_packets.begin(), udp_packets.end(),
						std::back_inserter(scan_packets),
						[udp_from_icmp, ip_from_icmp](
							std::shared_ptr<pckt_header> udp_packet) {
							auto udp_hdr =
								std::dynamic_pointer_cast<proto_header_udp>(
									(udp_packet)->proto);
							// XXX: IP header has different checksums! because
							// of ttl...
							// Need to check multiple fields because checksum
							// can be 0
							bool result =
								udp_from_icmp->uh_sum == udp_hdr->uh_sum &&
								udp_from_icmp->uh_dport == udp_hdr->uh_dport &&
								udp_from_icmp->uh_sport == udp_hdr->uh_sport &&
								ip_from_icmp->daddr == udp_packet->ip.daddr &&
								ip_from_icmp->saddr == udp_packet->ip.saddr;
							return result;
						});*/
				}
			}
		}
		return scan_packets;
	}

	/**
	 * Get the IPs of all udp/syn scanners
	 * @param packets
	 * @return map containing all <ips, scan counts>
	 */
	std::map<uint32_t, uint64_t> get_scanner(
		const std::vector<std::shared_ptr<pckt_header>>& packets) {
		std::map<uint32_t, uint64_t> result_map;
		for (auto iter = packets.begin(); iter != packets.end(); ++iter) {
			result_map[(*iter)->ip.saddr]++;
		}
		return result_map;
	}

	/**
	 * Get a map of all l4 protocols
	 * @return map containing <protocol, count>
	 */
	std::unordered_map<uint8_t, uint64_t> get_l4_protocols() const {
		std::unordered_map<uint8_t, uint64_t> protocols;
		for (auto iter = this->m_ip_headers.begin();
			 iter != this->m_ip_headers.end(); ++iter) {
			if ((*iter)->ip.version == 4) {
				protocols[(*iter)->ip.protocol]++;
			}
		}
		return protocols;
	}

	/**
	 * Get a map of all l3 protocols
	 * @return map containing <protocol, count>
	 */
	std::unordered_map<uint16_t, uint64_t> get_l3_protocols() const {
		std::unordered_map<uint16_t, uint64_t> protocols;
		for (auto iter = this->m_other_headers.begin();
			 iter != this->m_other_headers.end(); ++iter) {
			protocols[(*iter)->ether_type]++;
		}
		return protocols;
	}

	/**
	 * Find http auth packets
	 * @return <host, token>
	 */
	std::vector<std::tuple<std::string, std::string>> find_http_auth() {
		std::vector<std::tuple<std::string, std::string>> credentials;
		for (auto iter = this->m_ip_headers.begin();
			 iter != this->m_ip_headers.end(); ++iter) {
			auto tcp_hdr =
				std::dynamic_pointer_cast<proto_header_tcp>((*iter)->proto);

			if (tcp_hdr) {
				// only http on standard port
				if (::ntohs(tcp_hdr->th_dport) == 80) {
					auto data =
						std::static_pointer_cast<unsigned char>((*iter)->data);
					// XXX: hacky :/
					std::string str(reinterpret_cast<char*>(data.get()));
					const std::string search_term = "Authorization: Basic ";
					size_t pos = str.find(search_term);
					// Found header
					if (pos != str.npos) {
						size_t end = str.find("\r\n", pos);
						std::string base_64_credentials =
							str.substr(pos + search_term.size(),
									   end - pos - search_term.size());
						size_t host_pos = 0;
						size_t host_end = 0;
						const std::string host_search_string = "Host: ";
						host_pos = str.find(host_search_string);
						host_end = str.find("\r\n", host_pos);
						std::string host = str.substr(
							host_pos + host_search_string.size(),
							host_end - host_pos - host_search_string.size());

						credentials.push_back({host, base_64_credentials});
					}
				}
			}
		}
		return credentials;
	}

	static void packetHandler(u_char* userData,
							  const struct pcap_pkthdr* pkthdr,
							  const u_char* packet);

 private:
	uint64_t m_total_packets = 0;
	std::vector<std::shared_ptr<pckt_header>> m_ip_headers;
	std::vector<std::shared_ptr<ether_header>> m_other_headers;

	int m_link_type = 0;
};

#endif /* SRC_PCAP_ANALYZER_H_ */
