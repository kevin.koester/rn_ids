#include <arpa/inet.h>
#include <net/ethernet.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <netinet/ip_icmp.h>
#include <netinet/tcp.h>
#include <netinet/udp.h>
#include <pcap.h>
#include <pcap/bpf.h>
#include <iostream>
#include <map>
#include <sstream>
#include <vector>

#include "pcap_analyzer.h"

// Prototype that processes every packet
void packetHandler(u_char* userData, const struct pcap_pkthdr* pkthdr,
				   const u_char* packet);

std::string int_to_ip(uint32_t ipa) {
	uint32_t ip = ::ntohl(ipa);
	unsigned char bytes[4];
	bytes[0] = ip & 0xFF;
	bytes[1] = (ip >> 8) & 0xFF;
	bytes[2] = (ip >> 16) & 0xFF;
	bytes[3] = (ip >> 24) & 0xFF;

	return std::to_string(bytes[3]) + "." + std::to_string(bytes[2]) + "." +
		   std::to_string(bytes[1]) + "." + std::to_string(bytes[0]);
}

std::string get_top_scanner_str(
	const std::vector<std::tuple<uint32_t, uint64_t>>& scanner, int num) {
	std::stringstream top_scanner_output;
	int i = 0;
	for (auto iter = scanner.begin(); iter != scanner.end(); ++iter) {
		uint32_t ip = std::get<0>(*iter);
		std::string ip_str = int_to_ip(ip);

		top_scanner_output << ip_str << ": " << std::get<1>(*iter)
						   << " scans\n";

		if (++i == num) break;
	}
	return top_scanner_output.str();
}

std::string get_flow_str(ip_port_flow_t flow) {
	std::stringstream ss;
	auto source_ip_port = std::get<0>(flow);
	auto dest_ip_port = std::get<1>(flow);
	ss << "(" << int_to_ip(std::get<0>(source_ip_port)) << ":"
	   << ::ntohs(std::get<1>(source_ip_port)) << ", "
	   << int_to_ip(std::get<0>(dest_ip_port)) << ":"
	   << ::ntohs(std::get<1>(dest_ip_port)) << ")";
	return ss.str();
}
// Main of the programm
int main(int argc, char* argv[]) {
	// Read argument command line
	if (argc != 2 && argc != 3) {
		std::cout << "Use the file name as first command line argument plus "
					 "optionally the number of packets to process"
				  << std::endl;
		return 1;
	}
	// File name for the network trace
	std::string file_name = argv[1];
	// Number of packets to process
	int nPackets = 0;
	if (argc == 3) {
		nPackets = atoi(argv[2]);
	}

	PcapAnalyzer analyzer(file_name, nPackets);

	std::cout << "Analyzing...this might take some time..." << std::endl;
	auto susp_packages = analyzer.find_unsusual_packet_size();
	auto syn_scanner = analyzer.sort_scanners(analyzer.get_scanner(
		analyzer.get_syn_scanner_packets(analyzer.get_tcp_connections())));
	auto udp_scanner = analyzer.sort_scanners(
		analyzer.get_scanner(analyzer.get_udp_scanner_packets()));

	auto l3_protocols = analyzer.get_l3_protocols();

	std::stringstream l3_proto_stats_ss;
	for (auto iter = l3_protocols.begin(); iter != l3_protocols.end(); ++iter) {
		l3_proto_stats_ss << std::to_string(iter->first) << ": " << iter->second
						  << " packets\n";
	}

	auto l4_protocols = analyzer.get_l4_protocols();

	std::stringstream l4_proto_stats_ss;
	for (auto iter = l4_protocols.begin(); iter != l4_protocols.end(); ++iter) {
		l4_proto_stats_ss << std::to_string(iter->first) << ": " << iter->second
						  << " packets\n";
	}

	auto http_credentials = analyzer.find_http_auth();

	std::stringstream credentials_ss;
	if (http_credentials.size() == 0) {
		credentials_ss << "None found.\n";
	}
	for (auto iter = http_credentials.begin(); iter != http_credentials.end();
		 ++iter) {
		credentials_ss << "Http basic auth \"" << std::get<1>(*iter)
					   << "\" for " << std::get<0>(*iter) << "\n";
	}

	auto max_flow_func = [](const ip_port_flow_map_t& map) {
		std::tuple<ip_port_flow_t, size_t> max;
		std::get<1>(max) = 0;

		for (auto flow_iter = map.begin(); flow_iter != map.end();
			 ++flow_iter) {
			size_t bytes = 0;

			for (auto iter = flow_iter->second.begin();
				 iter != flow_iter->second.end(); ++iter) {
				bytes += ::ntohs((*iter)->ip.tot_len);
			}

			if (bytes > std::get<1>(max)) {
				max = {flow_iter->first, bytes};
			}
		}
		return max;
	};
	auto ip_flows = analyzer.get_ip_flows(PACKET_TYPE::IP);

	auto max_ip_flow = max_flow_func(ip_flows);
	auto tcp_udp_flows =
		analyzer.get_ip_flows(PACKET_TYPE::TCP | PACKET_TYPE::UDP);
	auto max_tcp_udp_flow = max_flow_func(tcp_udp_flows);

	std::cout
		<< "\nTotal packets:" << analyzer.get_total_packets()
		<< "\nIPv4 packets: " << analyzer.get_num_packets_ipv4()
		<< "\nUnique IPv4 addresses: " << analyzer.get_num_ips_ipv4()
		<< "\nNumber of different L3-Protocols (without IP): "
		<< l3_protocols.size() << "\nL3 protocol distribution:\n"
		<< l3_proto_stats_ss.str()
		<< "\nNumber of different L4-Protocols: " << l4_protocols.size()
		<< "\nL4 protocol distribution:\n"
		<< l4_proto_stats_ss.str() << "\nTotal payload size: "
		<< analyzer.get_total_payload_size() / 1024.0 / 1024.0 << " MiB"
		<< "\nTotal udp size: "
		<< analyzer.get_total_payload_size(PACKET_TYPE::UDP) / 1024.0 / 1024.0
		<< " MiB"
		<< "\nTotal tcp size: "
		<< analyzer.get_total_payload_size(PACKET_TYPE::TCP) / 1024.0 / 1024.0
		<< " MiB"
		<< "\nTotal other size: "
		<< analyzer.get_total_payload_size(PACKET_TYPE::OTHER) / 1024.0 / 1024.0
		<< " MiB"
		<< "\nNumber of ipv4 flows: " << ip_flows.size()
		<< "\nMost bytes on flow " << get_flow_str(std::get<0>(max_ip_flow))
		<< " with " << std::get<1>(max_ip_flow) / 1024.0 / 1024.0 << " MiB"
		<< "\nNumber of tcp/udp flows: " << tcp_udp_flows.size()
		<< "\nMost bytes on flow "
		<< get_flow_str(std::get<0>(max_tcp_udp_flow)) << " with "
		<< std::get<1>(max_tcp_udp_flow) / 1024.0 / 1024.0 << " MiB"
		<< "\nNumber of udp scanners: " << udp_scanner.size()
		<< "\nTop 5 udp scanner: \n"
		<< get_top_scanner_str(udp_scanner, 5)
		<< "\nNumber of syn scanners: " << syn_scanner.size()
		<< "\nTop 5 syn scanner: \n"
		<< get_top_scanner_str(syn_scanner, 5) << "\nHTTP Basic auth tokens: \n"
		<< credentials_ss.str() << susp_packages.size()
		<< " packets with wrong size." << std::endl;
}

void PcapAnalyzer::packetHandler(u_char* userData,
								 const struct pcap_pkthdr* pkthdr,
								 const u_char* packet) {
	PcapAnalyzer* context = reinterpret_cast<PcapAnalyzer*>(userData);

	++context->m_total_packets;

	size_t header_size = 0;
	if (context->m_link_type == DLT_EN10MB) {
		header_size = sizeof(ether_header);
		// Get Link Header (layer 2)
		const auto ethernetHeader =
			reinterpret_cast<const ether_header*>(packet);

		// Ignore non IP packets
		if (::ntohs(ethernetHeader->ether_type) != ETHERTYPE_IP) {
			context->m_other_headers.push_back(
				std::make_shared<ether_header>(*ethernetHeader));
			return;
		}
	} else if (context->m_link_type == DLT_NULL) {
		header_size = sizeof(uint32_t);
		// Get loopback header
		const auto loopback_header = reinterpret_cast<const uint8_t*>(packet);
		// Ignore non IP packets
		if (*loopback_header != 2) {
			return;
		}
	}

	const auto ip_header = reinterpret_cast<const iphdr*>(packet + header_size);
	size_t payload_start = header_size + sizeof(iphdr);

	// TODO: fragmentation
	// Save packets with mf flag set and group by id field

	// XXX: since we are (mostly) not interested in the payloads, just ignore
	// fragmented packages and don't count them as an ip packet
	if ((::ntohs(ip_header->frag_off) & IP_OFFMASK) != 0) {
		return;
	}

	// TODO: call every analyze function from here to speed up everything... low
	// prio
	pckt_header header;
	header.ip = *ip_header;
	if (ip_header->protocol == IPPROTO_TCP) {
		const tcphdr* tcp_header =
			reinterpret_cast<const tcphdr*>(packet + payload_start);
		header.proto = std::make_shared<proto_header_tcp>(*tcp_header);
		payload_start += sizeof(tcphdr);
	}
	if (ip_header->protocol == IPPROTO_UDP) {
		const udphdr* udp_header =
			reinterpret_cast<const udphdr*>(packet + payload_start);
		header.proto = std::make_shared<proto_header_udp>(*udp_header);
		payload_start += sizeof(udphdr);
	}
	if (ip_header->protocol == IPPROTO_ICMP) {
		const icmphdr* icmp_header =
			reinterpret_cast<const icmphdr*>(packet + payload_start);
		payload_start += sizeof(icmphdr);
		header.proto = std::make_shared<proto_header_icmp>(*icmp_header);
	}

	// XXX: use caplen here!
	size_t payload_len = pkthdr->caplen - payload_start;
	std::shared_ptr<uint8_t> data(new uint8_t[payload_len],
								  [](uint8_t* p) { delete[] p; });
	memcpy(data.get(), packet + payload_start, payload_len);
	header.data = data;
	header.total_ip_length = pkthdr->len - sizeof(ether_header);

	context->m_ip_headers.push_back(std::make_shared<pckt_header>(header));
}

PcapAnalyzer::PcapAnalyzer(const std::string& filename, const int max_packets) {
	char errbuf[PCAP_ERRBUF_SIZE];
	std::cout << "Loading file " << filename << std::endl;
	pcap_t* descr = pcap_open_offline(filename.c_str(), errbuf);

	if (!descr) {
		throw std::runtime_error(std::string("Couldn't open file ") + filename);
	}

	std::cout << "Loaded file with datalink type: " << pcap_datalink(descr)
			  << std::endl;

	if (pcap_datalink(descr) != DLT_EN10MB &&
		pcap_datalink(descr) != DLT_NULL) {
		throw std::runtime_error(
			std::string("Cannot process link layer type ") +
			std::to_string(pcap_datalink(descr)));
	}

	this->m_link_type = pcap_datalink(descr);

	if (pcap_loop(descr, max_packets, &PcapAnalyzer::packetHandler,
				  reinterpret_cast<u_char*>(this)) < 0) {
		throw std::runtime_error(std::string("pcap_loop() failed: ") +
								 pcap_geterr(descr));
	}

	pcap_close(descr);
}
